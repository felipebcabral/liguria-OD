# Conversation from XML to CSV for files from Liguria transparency portal.

This conversor was created to support https://operazionevetro.info/ and the analysis of the public data to identify and flag possible acts of misuse of public money. 

The Liguria data portal don't give the option to download datasets as Excel format or CSV with all the meaningful data, only through XML. 

To mini app convert the full data from the XML to CSV files. 

Growing fields are set as array inside the relative column. 


## Install libraries

### Poetry

```
$ poetry install
```

or through PIP

PIP

```
$ pip install -r requirements.txt
```

### Tests

Run tests with

```
$ poetry run pytest
```

## Project live

The project is live at Heroku, on a hobby tier.

Visit: https://herokuliguriaod.herokuapp.com/

### Testing live project

You can use the XML from the `tests/fixtures` or you can download a dataset from https://appaltiliguria.regione.liguria.it/pubblica/cicloappalti/cicloappalti/mostraAppaltiPubblici_frameset.jsp?trasparenza=si 