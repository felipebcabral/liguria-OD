import os

from django.http import HttpResponse
from django.shortcuts import redirect, render

from .forms import DocumentForm
from .models import Document
from .tasks import process


def index(request):
    message = ''
    # Handle file upload
    if request.method == 'POST':
        form = DocumentForm(request.POST, request.FILES)
        if form.is_valid():
            newdoc = form.save()
            try:
                # Create CSV file
                process(newdoc.pk)
                # Redirect to the document list after POST
                return redirect('index')
            except:
                message = 'The XML file is corrupted.'
        else:
            message = 'The form is not valid. Fix the following error:'
    else:
        form = DocumentForm() 

    # Load documents for the list page
    documents = Document.objects.all()
    documents = documents[5:]
    # Render list page with the documents and the form
    context = {
        'documents': documents,
        'form': form,
        'message': message}
    return render(request, 'upload.html', context)
