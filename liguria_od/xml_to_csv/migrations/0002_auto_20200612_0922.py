# Generated by Django 3.0.6 on 2020-06-12 09:22

from django.db import migrations, models
import liguria_od.xml_to_csv.validators


class Migration(migrations.Migration):

    dependencies = [
        ('xml_to_csv', '0001_initial'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='document',
            name='docfile',
        ),
        migrations.AddField(
            model_name='document',
            name='csv_file',
            field=models.FileField(default='test.csv', upload_to=''),
        ),
        migrations.AddField(
            model_name='document',
            name='xml_file',
            field=models.FileField(default='test.xml', upload_to='documents/%Y/%m/%d', validators=[liguria_od.xml_to_csv.validators.validate_file_extension]),
        ),
    ]
