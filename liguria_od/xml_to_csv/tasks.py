import os
from pathlib import Path
from tempfile import NamedTemporaryFile

from django.core.files.base import ContentFile

from liguria_od.xml_to_csv.converter import xml_parser
from liguria_od.xml_to_csv.models import Document


def process(pk):
    doc = Document.objects.get(pk=pk)

    with doc.xml_file.open() as f, NamedTemporaryFile() as t: 
        Path(t.name).write_bytes(f.read()) 
        file_wrapper = xml_parser(t.name) 

    doc.csv_file = file_wrapper.name
    base_dir = os.path.dirname(doc.xml_file.name) 
    filename = Path(doc.xml_file.name) 
    filename = filename.name.replace('.xml','.csv') 
    csv_name = base_dir + '/' + filename 
    
    with open(file_wrapper.name, 'rb') as f:
        content = f.read()

    doc.csv_file.storage.save(csv_name, ContentFile(content))   

    doc.csv_file.name = csv_name    
    doc.save()
