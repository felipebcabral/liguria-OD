import os

from django.core.exceptions import ValidationError

def validate_file_extension(value):   
    prefix, extension = os.path.splitext(value.name)
    valid_extensions = ['.xml']
    if not extension.lower() in valid_extensions:
        raise ValidationError('Unsupported file extension.')

def validate_xml_parser():
    pass

