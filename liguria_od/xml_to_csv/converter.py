import csv
import os
import xml.etree.ElementTree as ET


# List all items of all members of aggiudicatario nest
def member_listing(raggruppamento):
    dati_membri = '['
    for member in raggruppamento:
        for item in member:
            dati_membri += f'{item.tag}: {item.text},'
        dati_membri = dati_membri[:-1]  # Removes last comma
        dati_membri = dati_membri + '],['  # Format it for more than 1 entry
    aggiudicatario_membri = dati_membri[:-2]  # Clean last entry
    return dati_membri

# List items inside of a nest
def group_items(list_of_items):
    string = ''
    if list_of_items:
        for item in list_of_items:
            string += f'{item.text},'
        string = string[:-1]
        return string

def xml_parser(xml_file): 
    tree = ET.parse(xml_file)
    root = tree.getroot()

    # Lotto is the minimal unit of the data inside the Liguria XML file.
    lottos = tree.findall('.//lotto')

    # The columns that compose the CSV output file
    columns = (
        'cig',
        'oggetto',
        'sceltaContraente',
        'importoAggiudicazione',
        'importoSommeLiquidate',
        'strutturaProponente_codiceFiscaleProp',
        'strutturaProponente_denominazione',
        'tempiCompletamento_dataInizio',
        'tempiCompletamento_dataUltimazione',
        'aggiudicatario_codiceFiscale',
        'aggiudicatario_ragioneSociale',
        'aggiudicatario_membri',
    )

    rows = list()
    for children in lottos:
        cig = children.find('cig').text if children.find('cig') is not None else None
        oggetto = (
            children.find('oggetto').text
            if children.find('oggetto') is not None
            else None
        )
        sceltaContraente = (
            children.find('sceltaContraente').text
            if children.find('sceltaContraente') is not None
            else None
        )
        importoAggiudicazione = (
            children.find('importoAggiudicazione').text
            if children.find('importoAggiudicazione') is not None
            else None
        )
        importoSommeLiquidate = (
            children.find('importoSommeLiquidate').text
            if children.find('importoSommeLiquidate') is not None
            else None
        )
        strutturaProponente_codiceFiscaleProp = (
            children.find('strutturaProponente//codiceFiscaleProp').text
            if children.find('strutturaProponente//codiceFiscaleProp') is not None
            else None
        )
        strutturaProponente_denominazione = (
            children.find('strutturaProponente//denominazione').text
            if children.find('strutturaProponente//denominazione') is not None
            else None
        )
        tempiCompletamento_dataInizio = (
            children.find('tempiCompletamento//dataInizio').text
            if children.find('tempiCompletamento//dataInizio') is not None
            else None
        )
        tempiCompletamento_dataUltimazione = (
            children.find('tempiCompletamento//dataUltimazione').text
            if children.find('tempiCompletamento//dataUltimazione') is not None
            else None
        )

        aggiudicatario_codiceFiscale = (
            children.findall('aggiudicatari//aggiudicatario//codiceFiscale')
            if children.findall('aggiudicatari//aggiudicatario//codiceFiscale')
            is not None
            else None
        )
        identificativoFiscaleEstero = (
            children.findall(
                'aggiudicatari//aggiudicatario//identificativoFiscaleEstero'
            )
            if children.findall(
                'aggiudicatari//aggiudicatario//identificativoFiscaleEstero'
            )
            is not None
            else None
        )
        if identificativoFiscaleEstero:
            aggiudicatario_codiceFiscale = identificativoFiscaleEstero

        string = ''
        for item in aggiudicatario_codiceFiscale:
            string += f'{item.text},'
            string = string[:-1]  # Removes last comma 
        aggiudicatario_codiceFiscale = string

        aggiudicatario_ragioneSociale = (
            children.findall('aggiudicatari//aggiudicatario//ragioneSociale')
            if children.findall('aggiudicatari//aggiudicatario//ragioneSociale')
            is not None
            else None
        )

        string = ''
        for item in aggiudicatario_ragioneSociale:
            string += f'{item.text},'
            string = string[:-1]
        aggiudicatario_ragioneSociale = string

        aggiudicatario_membri = ''
        raggruppamento = (
            children.findall('aggiudicatari//aggiudicatarioRaggruppamento//membro')
            if children.findall('aggiudicatari//aggiudicatarioRaggruppamento//membro')
            is not None
            else None
        )
        if raggruppamento:
            aggiudicatario_membri = member_listing(raggruppamento)
            aggiudicatario_membri = aggiudicatario_membri[:-2]

        rows.append(
            {
                'cig': cig,
                'oggetto': oggetto,
                'sceltaContraente': sceltaContraente,
                'importoAggiudicazione': importoAggiudicazione,
                'importoSommeLiquidate': importoSommeLiquidate,
                'strutturaProponente_codiceFiscaleProp': strutturaProponente_codiceFiscaleProp,
                'strutturaProponente_denominazione': strutturaProponente_denominazione,
                'tempiCompletamento_dataInizio': tempiCompletamento_dataInizio,
                'tempiCompletamento_dataUltimazione': tempiCompletamento_dataUltimazione,
                'aggiudicatario_codiceFiscale': aggiudicatario_codiceFiscale,
                'aggiudicatario_ragioneSociale': aggiudicatario_ragioneSociale,
                'aggiudicatario_membri': aggiudicatario_membri,
            }
        )

    prefix, extension = os.path.splitext(xml_file)
    csv_output = prefix + '.csv'

    with open(csv_output, 'w', newline='') as file:
        writer = csv.DictWriter(file, fieldnames=columns)
        writer.writeheader()
        writer.writerows(rows)
        return file
