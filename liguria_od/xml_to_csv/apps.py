from django.apps import AppConfig


class XmlToCsvConfig(AppConfig):
    name = 'xml_to_csv'
