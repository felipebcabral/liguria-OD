import pytest

import csv
from tempfile import NamedTemporaryFile
from shutil import copy2

from liguria_od.xml_to_csv.converter import xml_parser


@pytest.fixture
def xml_path_fixture():
    with NamedTemporaryFile(
        suffix=".xml",
        prefix="xml_fix",
        ) as tmp_xml:
        
        xml_path = copy2(
            'liguria_od/xml_to_csv/tests/fixtures/dataset_liguria_digitale_2019.xml',
            tmp_xml.name
            )
        yield xml_path

@pytest.fixture
def csv_path_fixture():
    with NamedTemporaryFile(
        suffix=".csv",
        prefix="csv_fix",
        ) as tmp_csv:
        
        csv_path = copy2(
            'liguria_od/xml_to_csv/tests/fixtures/dataset_liguria_digitale_2019.csv',
            tmp_csv.name
        )
        yield csv_path
    
def test_document_loading(xml_path_fixture, csv_path_fixture):
    # Test files presence in /fixtures and if we can load them on tmp dir
    assert xml_path_fixture
    assert csv_path_fixture

def test_data_transformation(xml_path_fixture):
    # Test if can transform a XML to CSV
    csv_output = xml_parser(xml_path_fixture)
    assert csv_output

def test_csv_structure(xml_path_fixture):
    # Test if the first line of the CSV gives the expected results

    expectec_result = {
        "cig": "7431888BFB",
        "oggetto": "SERVIZIO DI ASSISTENZA E CONDUZIONE DELLE APPLICAZIONI SOCIO SANITARIE, ATTIVITÃ\x80 SPECIFICHE PER L'ANAGRAFE AZIENDALE DEI CONTATTI, SUPPORTO E FORMAZIONE",
        "sceltaContraente": "01-PROCEDURA APERTA",
        "importoAggiudicazione": "517470.00",
        "importoSommeLiquidate": "98300.77",
        "strutturaProponente_codiceFiscaleProp": "02994540108",
        "strutturaProponente_denominazione": "Liguria Digitale SpA",
        "tempiCompletamento_dataInizio": "2019-03-06",
        "tempiCompletamento_dataUltimazione": "",
        "aggiudicatario_codiceFiscale": "",
        "aggiudicatario_ragioneSociale": "",
        "aggiudicatario_membri": "[codiceFiscale: 03629340104,ragioneSociale: T & G TECHNOLOGY & GROUPWARE,ruolo: 02-MANDATARIA],[codiceFiscale: 00488410010,ragioneSociale: TELECOM ITALIA S.P.A.,ruolo: 01-MANDANTE]",
    }

    csv_fixture = (
        "liguria_od/xml_to_csv/tests/fixtures/dataset_liguria_digitale_2019.csv"
    )

    csv_output = xml_parser(xml_path_fixture)

    with open(csv_output.name) as file:
        data = csv.DictReader(file)
        structure_data = [row for row in data]
 
    assert structure_data[0] == expectec_result

