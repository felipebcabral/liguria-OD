import pytest

from django import forms
from django.core.files.uploadedfile import SimpleUploadedFile
from liguria_od.xml_to_csv.forms import DocumentForm


@pytest.mark.parametrize(
    "xml_file, validity",
    [
        ("file.xml", True),
        ("file.jpg", False), 
        ("file.csv", False), 
        ("", False),
    ],
)

def test_documentform(xml_file, validity):
    form = DocumentForm(
        data={},
        files={"xml_file": SimpleUploadedFile(xml_file, b"content")},
        auto_id=False,
    )
    
    assert form.is_valid() is validity

