import pytest

from liguria_od.xml_to_csv.models import Document

@pytest.fixture
def document_model():
    doc = Document.objects.create(xml_file="test.xml", csv_file="test.csv")
    return doc 

@pytest.mark.django_db
def test_document_creation():
    assert not Document.objects.count()
    Document.objects.create(xml_file="test.xml", csv_file="test.csv")
    assert Document.objects.count() == 1

@pytest.mark.django_db
def test_document_proprieties(document_model):
    assert Document.objects.count() == 1
    assert Document.xml_file
    assert Document.csv_file


