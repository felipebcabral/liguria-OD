from django.db import models

from .validators import validate_file_extension


class Document(models.Model):
    xml_file = models.FileField(
        upload_to='documents/%Y/%m/%d',
        validators=[validate_file_extension])
    csv_file = models.FileField()
    
